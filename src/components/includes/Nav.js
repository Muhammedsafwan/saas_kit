import React from 'react'
import styled from "styled-components";
import notification from "../../assets/images/notifications.png"
import User from "../../assets/images/user.png"
import DashboardIcon from '../../assets/images/dashboard-active.png';
import Sett from "../../assets/images/settings.png"
import Search from "../../assets/images/search.png"
import Contact from "../../assets/images/contacts.png"
import {Link} from "react-router-dom"
import Togg from "../../assets/images/toggle.png"


export default function Nav() {
    return (
        <>
            <section id="main">
                <div className='left'>
                    <div className='left-top'>
                        <Link to="/" >
                            <h1>SaaS Kit</h1>
                        </Link>
                    </div>
                    <div className='left-middle'>
                        <div className='profile'>
                            <div className='profile-left'>
                                <div className='img-box'>
                                    <img src={User} alt="user" />
                                </div>
                            </div>
                            <div className='profile-right'>
                                <h5 className='name'>
                                    Sierra Ferguson
                                </h5>
                                <p>s.ferguson@gmail.com</p>
                            </div>
                        </div>
                        <div className='categories'>
                            <ul>
                            <Link to="/" >
                                <li>
                                    <div className='img-box'>
                                        <img src={DashboardIcon} alt="ds" />
                                    </div>
                                    <h5 className='blue'>Dashboard</h5>
                                </li>
                                </Link>
                                <Link to="/contact" >
                                <li>
                                    <div className='img-box'>
                                        <img src={Contact} alt="ds" />
                                    </div>
                                    <h5>Contact</h5>
                                </li>
                                </Link>
                            </ul>
                        </div>
                        <div className='settings'>
                            <div className='img-box'>
                                <img src={Sett} alt="ds" />
                            </div>
                            <h5>Settings</h5> 
                        </div>
                    </div>
                </div>
                <div className='right-main'>
                    <div className='top'>
                        <div className='right-top-left'>
                            <img src={Search} alt="f" />
                        </div>
                        <form>
                            <input type="text" placeholder='Search'/>
                        </form>
                        <div className='right'>
                            <img src={notification} alt="h" />
                        </div>
                    </div>
                    <div className='components-route'>
                        
                    </div>
                </div>
            </section>
        </>

        // <NavBox>
          
        //     <LeftBox>
        //         <LeftContent>
        //             <Title>SaaS Kit</Title>
        //         </LeftContent>
        //         <Top>
        //             <TopImageContainer>
        //                 <TopImage src={user} alt="Image" />
        //             </TopImageContainer>
        //             <TopContent>
        //                 <TopContentTitle>Sierra Ferguson</TopContentTitle>
        //                 <TopContentPara>s.ferguson@gmail.com</TopContentPara>
        //             </TopContent>
        //         </Top>
        //         <Bottom>
        //             <BottomNav>
        //                 <BottomList>
        //                     <Icon>
        //                     <IconImage src={DashboardIcon} alt="Image" />
        //                     </Icon>

        //                     <BottomTitle to="/dashboard">Dashboard</BottomTitle>
        //                 </BottomList>
        //                 <BottomList>
        //                     <Icon>
        //                     <IconImage src={Contact} alt="Image" />
        //                     </Icon>
        //                     <BottomTitle to="/contact">Contact</BottomTitle>
        //                 </BottomList>
        //             </BottomNav>
        //         </Bottom>
        //     </LeftBox>
        //     <SearchBar>
        //         <Form>
        //             <InputSearch type="text" placeholder="Global Search" />
        //         </Form>
        //         <IconContainer>
        //             <BellImage src={notification} alt="Image" />
        //         </IconContainer>
        //     </SearchBar>
        //     <SearchBa>
        //         <Form>
        //             <InputSearch type="text" placeholder="Global Search" />
        //         </Form>
        //         <IconContainer>
        //             <BellImage src={notification} alt="Image" />
        //         </IconContainer>
        //     </SearchBa>
            
        // </NavBox>
    );
}
// const SearchBa = styled.div`
// `
// const NavBox = styled.div`
// `;
// const LeftBox = styled.aside`
//     width: 20%;
//     height: 100vh;
//     /* position: absolute; */
//     top: 0;
//     box-shadow: 9px 6px 46px -24px rgba(0, 0, 0, 0.75);
//     position: fixed;
// `;

// const LeftContent = styled.div`
//     padding: 10px;
//     color: #109cf1;
//     margin-left: 20px;
// `;
// const Title = styled.h1``;
// const Top = styled.div`
//     display: flex;
//     align-items: center;
//     justify-content: space-evenly;
//     margin-top: 25px;
// `;
// const TopImageContainer = styled.div`
//     width: 25%;
// `;
// const TopImage = styled.img`
//     border-radius: 50%;
// `;
// const TopContent = styled.div``;
// const TopContentTitle = styled.h2`
//     font-size: 18px;
// `;
// const TopContentPara = styled.p`
//     margin: 0px;
//     color: grey;
//     font-size: 12px;
//     font-weight: 600;
// `;
// const Bottom = styled.div`
//     margin-left: 20px;
// `;
// const BottomNav = styled.ul`
//     padding: 0px;
// `;
// const BottomList = styled.li`
//     display: flex;
//     margin-top: 30px;
//     cursor: pointer;
//     transition: 0.4s ease-in-out;
// `;
// const Icon = styled.div`
//     width: 10%;
// `;
// const IconImage = styled.img``;
// const BottomTitle = styled(Link)`
//     margin-left: 10px;
//     color: #626466;
//     font-size: 18px;
//     &:hover {
//         color: #109cf1;
//         transition: 0.4s ease-in-out;
//     }
// `;
// const SearchBar = styled.header`
//     height: 64px;
//     width: 100%;
//     z-index: -1;
//     display: flex;
//     align-items: center;
//     border-bottom: 1px solid #ebeff2;
//     position: fixed;
//     background: #fff;
// `;
// const Form = styled.form`
//     padding-left: 20%;
// `;
// const InputSearch = styled.input`
//     padding: 21px 20px;
//     width: 800px;
//     border: 1px solid #fff;
//     ::placeholder {
//         color: #ebeff2;
//     }
// `;
// const IconContainer = styled.div`
//     margin-left: 18%;
//     cursor: pointer;
// `;
// const BellImage = styled.img``;



